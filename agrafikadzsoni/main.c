#include <GL/glut.h>
#include <stdio.h>
#include <math.h>

#include "draw.h"
#include "camera.h"
#include "model.h"
#include "texture_loader.h"

#define VIEWPORT_RATIO (21.0 / 9.0)
#define VIEWPORT_ASPECT 50.0

#define CAMERA_SPEED 5

/* camera */

int mouse_x, mouse_y;

struct Camera camera;

struct Action
{
	int step_forward;
	int step_backward;
	int step_left;
	int step_right;
	int step_up;
	int step_down;
	int increase_light;
	int decrease_light;
	int push_forward;
	int push_backward;
	int push_left;
	int push_right;
};

struct Action action;
int time;

/* model*/

struct Model model;

/*lights*/

GLfloat light_ambient[] = {1.0, 1.0, 1.0, 1.0};
GLfloat light_diffuse[] = {0.1, 0.1, 0.1, 1.0};
GLfloat light_specular[] = {0.6, 0.6, 0.6, 1.0};
GLfloat light_position[] = {3, 0, -10, 0};

/*cylinders*/

GLUquadric* qobj1;
GLUquadric* qobj2;
GLUquadric* qobj3;

/*animations*/

GLfloat push_speed = 0.008;

GLfloat push_forward_backward1 = 0.0f;
GLfloat push_left_right1 = 0.0f;

GLfloat push_forward_backward2 = 1.0f;
GLfloat push_left_right2 = 1.0f;

GLfloat push_forward_backward3 = -1.0f;
GLfloat push_left_right3 = -1.0f;

int dont_move_forward1 = 0;
int dont_move_backward1 = 0;
int dont_move_left1 = 0;
int dont_move_right1 = 0;

int dont_move_forward2 = 0;
int dont_move_backward2 = 0;
int dont_move_left2 = 0;
int dont_move_right2 = 0;

int dont_move_forward3 = 0;
int dont_move_backward3 = 0;
int dont_move_left3 = 0;
int dont_move_right3 = 0;

int which_figure = 1;

/*changing sugar*/

float sugar = 0.2;

/*help*/

int help_on = 0;

/*a kezdet kezdete*/

void light_up(){
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
}

void light_down(){
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
}

void change_radius(){
	printf("\nChange the radius of the figures if you want to, just write a number in, default is 0.2\n");
	scanf("%f", &sugar);
}

void draw_figure(){
	if(sugar<=0 || sugar>=1.21){
		printf("\n0 and smaller radius or 1.3 and bigger radius is not permitted!\n");
		sugar = 0.2;
	}
	glPushMatrix();
		if(which_figure == 1) glTranslatef(push_forward_backward1, push_left_right1, 0.0f);			//animacio
		else if(which_figure == 2 || which_figure == 3){
			glTranslatef(push_forward_backward1, push_left_right1, 0.0f);
		}
			glPushMatrix();
				glTranslatef(0.0f, 0.25f, 0.0001f);            										//magassaga a tablatol
				gluCylinder(qobj1, sugar, sugar, 0.20, 30, 16);
					glPushMatrix();
					glTranslatef(0.0f, 0.0f, 0.20f);												//3 h fedolap helye
					gluDisk(qobj1, 0.0f, sugar, 30, 1);												//3 r fedolap
					glPopMatrix();
					glPushMatrix();
					glRotatef(180, 1.0, 0, 0);														//alsolap lefele forgatasa
					gluDisk(qobj1, 0.0f, sugar, 30, 1); 											//3 r alsolap
					glPopMatrix();		
				//gluDeleteQuadric(qobj); 															//torlese
			glPopMatrix();
	glPopMatrix();
	
	glPushMatrix();
		if(which_figure == 2) glTranslatef(push_forward_backward2, push_left_right2, 0.0f);			//animacio
		else if(which_figure == 1 || which_figure == 3){
			glTranslatef(push_forward_backward2, push_left_right2, 0.0f);
		}
			glPushMatrix();
				glTranslatef(0.0f, 0.25f, 0.0001f);            										//magassaga a tablatol
				gluCylinder(qobj2, sugar, sugar, 0.20, 30, 16);
					glPushMatrix();
					glTranslatef(0.0f, 0.0f, 0.20f);												//3 h fedolap helye
					gluDisk(qobj2, 0.0f, sugar, 30, 1);												//3 r fedolap
					glPopMatrix();
					glPushMatrix();
					glRotatef(180, 1.0, 0, 0);														//alsolap lefele forgatasa
					gluDisk(qobj2, 0.0f, sugar, 30, 1); 											//3 r alsolap
					glPopMatrix();		
				//gluDeleteQuadric(qobj); 															//torlese
			glPopMatrix();
	glPopMatrix();
	
	glPushMatrix();
		if(which_figure == 3) glTranslatef(push_forward_backward3, push_left_right3, 0.0f);			//animacio
		else if(which_figure == 2 || which_figure == 1){
			glTranslatef(push_forward_backward3, push_left_right3, 0.0f);
		}
			glPushMatrix();
				glTranslatef(0.0f, 0.25f, 0.0001f);            										//magassaga a tablatol
				gluCylinder(qobj3, sugar, sugar, 0.20, 30, 16);
					glPushMatrix();
					glTranslatef(0.0f, 0.0f, 0.20f);												//3 h fedolap helye
					gluDisk(qobj3, 0.0f, sugar, 30, 1);												//3 r fedolap
					glPopMatrix();
					glPushMatrix();
					glRotatef(180, 1.0, 0, 0);														//alsolap lefele forgatasa
					gluDisk(qobj3, 0.0f, sugar, 30, 1); 											//3 r alsolap
					glPopMatrix();		
				//gluDeleteQuadric(qobj); 															//torlese
			glPopMatrix();
	glPopMatrix();
}

void keep_figure_on_board(){
	if (push_forward_backward1 >= 1.8-sugar+0.2) { dont_move_forward1 = 1; push_forward_backward1 = 1.799-sugar+0.2;}     	//up
	if (push_forward_backward1 <= -1.7+sugar-0.2) { dont_move_backward1 = 1; push_forward_backward1 = -1.699+sugar-0.2;} 	//down
	if (push_left_right1 >= 1.6-sugar+0.2) { dont_move_left1 = 1; push_left_right1 = 1.599-sugar+0.2;}				  		//left
	//printf("bal: %.2f\n",push_left_right );
	if (push_left_right1 <= -2.05+sugar-0.2) { dont_move_right1 = 1; push_left_right1 = -2.049+sugar-0.2; }			  		//right
	
	if (push_forward_backward1 <= 1.799-sugar+0.2) { dont_move_forward1 = 0;}
	if (push_forward_backward1 >= -1.699+sugar-0.2) { dont_move_backward1 = 0;}
	if (push_left_right1 <= 1.599-sugar+0.2) { dont_move_left1 = 0;}
	if (push_left_right1 >= -2.049+sugar-0.2) { dont_move_right1 = 0;}
	
	
	if (push_forward_backward2 >= 1.8-sugar+0.2) { dont_move_forward2 = 1; push_forward_backward2 = 1.799-sugar+0.2;}     	//up
	if (push_forward_backward2 <= -1.7+sugar-0.2) { dont_move_backward2 = 1; push_forward_backward2 = -1.699+sugar-0.2;} 	//down
	if (push_left_right2 >= 1.6-sugar+0.2) { dont_move_left2 = 1; push_left_right2 = 1.599-sugar+0.2;}				  		//left
	//printf("bal: %.2f\n",push_left_right );
	if (push_left_right2 <= -2.05+sugar-0.2) { dont_move_right2 = 1; push_left_right2 = -2.049+sugar-0.2; }			  		//right

	if (push_forward_backward2 <= 1.799-sugar+0.2) { dont_move_forward2 = 0;}
	if (push_forward_backward2 >= -1.699+sugar-0.2) { dont_move_backward2 = 0;}
	if (push_left_right2 <= 1.599-sugar+0.2) { dont_move_left2 = 0;}
	if (push_left_right2 >= -2.049+sugar-0.2) { dont_move_right2 = 0;}
	
	
	if (push_forward_backward3 >= 1.8-sugar+0.2) { dont_move_forward3 = 1; push_forward_backward3 = 1.799-sugar+0.2;}     	//up
	if (push_forward_backward3 <= -1.7+sugar-0.2) { dont_move_backward3 = 1; push_forward_backward3 = -1.699+sugar-0.2;} 	//down
	if (push_left_right3 >= 1.6-sugar+0.2) { dont_move_left3 = 1; push_left_right3 = 1.599-sugar+0.2;}				  		//left
	//printf("bal: %.2f\n",push_left_right );
	if (push_left_right3 <= -2.05+sugar-0.2) { dont_move_right3 = 1; push_left_right3 = -2.049+sugar-0.2; }			  		//right
	
	if (push_forward_backward3 <= 1.799-sugar+0.2) { dont_move_forward3 = 0;}
	if (push_forward_backward3 >= -1.699+sugar-0.2) { dont_move_backward3 = 0;}
	if (push_left_right3 <= 1.599-sugar+0.2) { dont_move_left3 = 0;}
	if (push_left_right3 >= -2.049+sugar-0.2) { dont_move_right3 = 0;}
}

void push_forward() {
	if(which_figure == 1)push_forward_backward1 += push_speed;
	if(which_figure == 2)push_forward_backward2 += push_speed;
	if(which_figure == 3)push_forward_backward3 += push_speed;
}

void push_backward() {
	if(which_figure == 1)push_forward_backward1 += -push_speed;
	if(which_figure == 2)push_forward_backward2 += -push_speed;
	if(which_figure == 3)push_forward_backward3 += -push_speed;
}

void push_left() {
	if(which_figure == 1)push_left_right1 += push_speed;
	if(which_figure == 2)push_left_right2 += push_speed;
	if(which_figure == 3)push_left_right3 += push_speed;
}

void push_right() {
	if(which_figure == 1)push_left_right1 += -push_speed;
	if(which_figure == 2)push_left_right2 += -push_speed;
	if(which_figure == 3)push_left_right3 += -push_speed;
}

void update_camera_position(struct Camera* camera, double elapsed_time)
{
	double distance;
	distance = elapsed_time * CAMERA_SPEED;

	if (action.step_forward == TRUE) {
		move_camera_forward(camera, distance);
	}

	if (action.step_backward == TRUE) {
		move_camera_backward(camera, distance);
	}

	if (action.step_left == TRUE) {
		move_camera_left(camera, distance);
	}

	if (action.step_right == TRUE) {
		move_camera_right(camera, distance);
	}

	if (action.step_up == TRUE) {
		move_camera_up(camera, distance);
	}

	if (action.step_down == TRUE) {
		move_camera_down(camera, distance);
	}

	if (action.increase_light == TRUE) {
		if (light_ambient[0] < 1.0) {
			light_ambient[0] += 0.009;
			light_ambient[1] += 0.009;
			light_ambient[2] += 0.009;
			light_ambient[3] += 0.009;
			//printf("0: %.2f, 1: %.2f, 2: %.2f\n", light_ambient[0], light_ambient[1], light_ambient[2]);
		}
		light_up();
	}

	if (action.decrease_light == TRUE) {
		if (light_ambient[0] > -0.80) {
			light_ambient[0] -= 0.009;
			light_ambient[1] -= 0.009;
			light_ambient[2] -= 0.009;
			light_ambient[3] -= 0.009;
			//printf("0: %.2f, 1: %.2f, 2: %.2f\n", light_ambient[0], light_ambient[1], light_ambient[2]);
		}
		light_down();
	}
	
	if (action.push_forward == TRUE) {
		if(dont_move_forward1 == 0)
		push_forward();
		if(dont_move_forward2 == 0)
		push_forward();
		if(dont_move_forward3 == 0)
		push_forward();
	}
	
	if (action.push_backward == TRUE) {
		if(dont_move_backward1 == 0)
		push_backward();
		if(dont_move_backward2 == 0)
		push_backward();
		if(dont_move_backward3 == 0)
		push_backward();
	}
	
	if (action.push_left == TRUE) {
		if(dont_move_left1 == 0)
		push_left();
		if(dont_move_left2 == 0)
		push_left();
		if(dont_move_left3 == 0)
		push_left();
	}
	
	if (action.push_right == TRUE) {
		if(dont_move_right1 == 0)
		push_right();
		if(dont_move_right2 == 0)
		push_right();
		if(dont_move_right3 == 0)
		push_right();
	}
}

double calc_elapsed_time()
{
    int current_time;
    double elapsed_time;
    current_time = glutGet(GLUT_ELAPSED_TIME);
    elapsed_time = (double)(current_time - time) / 1000.0;
    time = current_time;
    return elapsed_time;
}

void reshape(GLsizei width, GLsizei height)
{
    int x, y, w, h;
    double ratio;

    ratio = (double)width / height;
    if (ratio > VIEWPORT_RATIO) {
        w = (int)((double)height * VIEWPORT_RATIO);
        h = height;
        x = (width - w) / 2;
        y = 0;
    }
    else {
        w = width;
        h = (int)((double)width / VIEWPORT_RATIO);
        x = 0;
        y = (height - h) / 2;
    }

    glViewport (x, y, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(50.0, (GLdouble)width / (GLdouble)height, 0.01, 10000.0);
}

void drawText(const char *text, int lenght, int x, int y){
	glMatrixMode(GL_PROJECTION);
	double matrix[16];
	glGetDoublev(GL_PROJECTION_MATRIX,matrix);
	glLoadIdentity();
	glOrtho(0,800,0,600,-5,5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glLoadIdentity();
	glRasterPos2i(x,y);
	for(int i=0; i<lenght; i++){
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, (int)text[i]);
	}
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(matrix);
	glMatrixMode(GL_MODELVIEW);
}

void display()
{
	if (!help_on) {	
		double elapsed_time;
		elapsed_time = calc_elapsed_time();
		update_camera_position(&camera, elapsed_time);
		set_view_point(&camera);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		
		/*board model*/
		glPushMatrix();
			glRotatef(90, 1.0, 0, 0);
			glRotatef(90, 0, 1.0, 0);
			draw_model(&model);
		glPopMatrix();
		
		keep_figure_on_board();
		draw_figure();
		
	}else {
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		GLfloat light_help_ambient[] = {1.0, 1.0, 1.0, 1.0};
		GLfloat light_help_diffuse[] = {0.1, 0.1, 0.1, 1.0};
		GLfloat light_help_specular[] = {0.6, 0.6, 0.6, 1.0};
		glLightfv(GL_LIGHT0, GL_AMBIENT, light_help_ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, light_help_diffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, light_help_specular);
		
		char helpRow1[] = "Move around with (WASD)";
		char helpRow2[] = "Look around with (Mouse), but beware its a bit sensitive";
		char helpRow3[] = "Change intensity of light with (+) and (-)";
		char helpRow4[] = "Move figures with (IJKL)";
		char helpRow5[] = "Change selected figure to move, with (Enter)";
		char helpRow6[] = "Change figure size with (R), you need to type a number into the console";
		
		drawText(helpRow1,23,30,600);
		drawText(helpRow2,56,30,580);
		drawText(helpRow3,42,30,560);
		drawText(helpRow4,24,30,540);
		drawText(helpRow5,44,30,520);
		drawText(helpRow6,71,30,500);
	}
    glutSwapBuffers();
}

void specialFunc(int key) {
	switch (key) {
	case GLUT_KEY_F1:
		if(help_on == 1) {
			help_on=0;
		}else help_on = 1;
	}
}

void mouse_handler(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
}

void motion_handler(int x, int y)
{
	double horizontal, vertical;

	horizontal = mouse_x - x;
	vertical = mouse_y - y;

	rotate_camera(&camera, horizontal, vertical);

	mouse_x = x;
	mouse_y = y;

    glutPostRedisplay();
}

void key_handler(int key)
{
	switch (key) {
	case 'w':
        action.step_forward = TRUE;
		break;
	case 's':
        action.step_backward = TRUE;
		break;
	case 'a':
        action.step_left = TRUE;
		break;
	case 'd':
        action.step_right = TRUE;
		break;
	case 32:
        action.step_up = TRUE;
		break;
	case 'c':
        action.step_down = TRUE;
		break;	
	case 43: //+
        action.increase_light = TRUE;
		break;
	case 45: //-
        action.decrease_light = TRUE;
		break;
	case 'i': 
        action.push_forward = TRUE;
		break;
	case 'k': 
        action.push_backward = TRUE;
		break;
	case 'j': 
        action.push_left = TRUE;
		break;
	case 'l': 
        action.push_right = TRUE;
		break;
	case 'r': 
		change_radius();
		break;	
	case 13:				//enter
		which_figure++;
		if(which_figure > 3) which_figure = 1;
		break;
	case 27:
        exit(0);
	}
	glutPostRedisplay();
}

void key_up_handler(int key)
{
	switch (key) {
	case 'w':
        action.step_forward = FALSE;
		break;
	case 's':
        action.step_backward = FALSE;
		break;
	case 'a':
        action.step_left = FALSE;
		break;
	case 'd':
        action.step_right = FALSE;
		break;
	case 32:
        action.step_up = FALSE;
		break;
	case 43: //+
        action.increase_light = FALSE;
		break;
	case 45: //-
        action.decrease_light = FALSE;
		break;
	case 'c':
        action.step_down = FALSE;
		break;
	case 'i': 
        action.push_forward = FALSE;
		break;
	case 'k': 
        action.push_backward = FALSE;
		break;
	case 'j': 
        action.push_left = FALSE;
		break;
	case 'l': 
        action.push_right = FALSE;
		break;
	}
	glutPostRedisplay();
}

void idle()
{
    glutPostRedisplay();
}

void initialize()
{
	init_camera(&camera);
	glShadeModel(GL_SMOOTH);

	glEnable(GL_NORMALIZE);
	glEnable(GL_AUTO_NORMAL);
	
	glEnable(GL_CULL_FACE);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_TEXTURE_2D);
    
	glClearDepth(1.0);
	
	qobj1 = gluNewQuadric();
	qobj2 = gluNewQuadric();
	qobj3 = gluNewQuadric();
	gluQuadricNormals(qobj1, GLU_SMOOTH);
	gluQuadricNormals(qobj2, GLU_SMOOTH);
	gluQuadricNormals(qobj3, GLU_SMOOTH);
	
	initialize_texture("textures/Board.png");
	
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	
	glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
	
}

int main(int argc, char* argv[])
{
	int window;
	glutInit(&argc, argv);

	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1920, 1080);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	window = glutCreateWindow("Master-Builder of Great Happiness");
	glutSetWindow(window);

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(key_handler);
    glutKeyboardUpFunc(key_up_handler);
    glutMouseFunc(mouse_handler);
    glutMotionFunc(motion_handler);
    glutIdleFunc(idle);
	glutTimerFunc(1000 / 60, idle, 0);
	glutSpecialFunc(specialFunc);

    action.step_forward = FALSE;
    action.step_backward = FALSE;
    action.step_left = FALSE;
    action.step_right = FALSE;
	action.step_up = FALSE;
	action.step_down = FALSE;
	action.increase_light = FALSE;
	action.decrease_light = FALSE;
	action.push_forward= FALSE;
	action.push_backward= FALSE;
	action.push_left= FALSE;
	action.push_right= FALSE;
	
	load_model("obj/Board.obj", &model);
	scale_model(&model, 0.05, 0.05, 0.05);
	print_bounding_box(&model);
	initialize();
	
    glutMainLoop();

    return 0;
}